// global string tables/pages
var PROMO = "promotable";
var LOYALTY = "loyaltytable";
var PRODUCT = "producttable";
var ABOUT = "settings";
var SOCIALS = "socialtable";
var LOCATION = "loctable";
var NEWS = "posttable";

var SELECTED_TABLE = "";
// iframe messages
var activateSync = function(){ parent.postMessage('[{"keyEvent": "activateSync"}]','*') }; 
var activateHome = function(){ parent.postMessage('[{"keyEvent": "activateHome"}]','*') }; 
var disableSync = function(){ parent.postMessage('[{"keyEvent": "disableSync"}]','*') };
var init = function(fileName){ SELECTED_TABLE = fileName; parent.postMessage('[{"keyEvent": "ready", "fileName": "'+fileName+'"}]', '*');};
var initWithLogin = function(fileName){ SELECTED_TABLE = fileName; parent.postMessage('[{"keyEvent": "checkLoginStatus", "fileName": "'+fileName+'"}]', '*');};
var reInit = function(){ parent.postMessage('[{"keyEvent": "sync", "fileName": "'+SELECTED_TABLE+'"}]', '*');};
