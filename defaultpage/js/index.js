// Global Selectors 
var $iframe;
var $body;
$(function(){
	$iframe = $("iframe");
	$body = $("body");
	addEventListeners();
	initializeDOB();

});

function addEventListeners(){
	FastClick.attach( document.body ); // removes 300ms delay

	$(".navigation ul").on('click', navigate)
	$("header .back-btn").on('click',backs);
	$("header .home-btn").on('click',returnHome);
	$("header .sync-btn").on('click',sync);

	$("#showReg").on('click', function(e){ $body.addClass('show-reg'); $("input").attr('readonly', 'true'); setTimeout(function(){ $("input").removeAttr('readonly'); },600); });

	$("#login").on('click', login);
	$("#register").on('click', register);
	
	// Listen to message from iframe window
	window.addEventListener("message",messageHandler,false);

	//goto next tabindex when enter
	$('input').on('keydown', function(e){
		if(e.which === 13) {
			e.preventDefault();
			var $next = $('[tabIndex=' + (+this.tabIndex + 1) + ']');
			console.log($next.length);
			if (!$next.length) {
				$next = $('[tabIndex=1]');
			}
			$next.focus();
		}
	});
}

function messageHandler(e){
	var message = JSON.parse(e.data);
	var keyEvent = message[0].keyEvent;
	switch(keyEvent){
		case 'ready': // getting data
			var data = nativeInterface.getJSON(message[0].fileName);
			// var data = [{"campaign": [{"loyaltyID": "LOYGE188151DlbRl","name": "Free Coffee","points": "10","frequency": "0","description": "<p>123</p>\n","terms": "testing","promoType": "snap"},{"loyaltyID": "LOYmr16955GILwhZ","name": "Cornetto 3 + 1","points": "0","frequency": "3","description": "<p>Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App./p>\n","terms": "Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App.","promoType": "frequency"},{"loyaltyID": "LOY5e151153dc8mf","name": "Aloha Burger","points": "10","frequency": "0","description": "<p>A big sarap burger</p>\n","terms": "secret","promoType": "snap"},{"loyaltyID": "LOYZE1612386j6yQ","name": "FREE Tumbler and 12oz Nescafe Shake","points": "0","frequency": "3","description": "<p>Get a FREE White Chocolate on your 3rd purchse of any Cornetto using your Snap App./p>\n","terms": "","promoType": "frequency"},{"loyaltyID": "LOYQx161338TE43Y","name": "Sola 2 + 1","points": "2","frequency": "0","description": "<p>Test</p>\n","terms": "Test","promoType": "snap"}]},{"profile": {"raffleEntry": 0,"totalPoints": 0,"fname": "Bat","mname": "","lname": "Saver","email": "albert.madia@yahoo.com","mobileNum": "","landlineNum": "","dateReg": "2015-03-24","qrApp": "","memberID": "MEMYl121217GQZLC","redeemable_sku_promo": [{"loyaltyID": "LOYmr16955GILwhZ","count": 0,"group": "Selecta Cornetto"},{"loyaltyID": "LOYZE1612386j6yQ","count": 0,"group": "Nescafe Shake"}],"lastSync": "Oct 16,2015"}}]
;

			// var data = [];
			$(".sync-btn")[0].setAttribute('data-sync', message[0].fileName);
			$("header").addClass("active-sync");
			// $iframe[0].contentWindow.postMessage('[{"keyEvent": "jsonData"},{"jsonData": '+JSON.stringify(data)+'}]', '*');
			$iframe[0].contentWindow.postMessage('[{"keyEvent": "jsonData"},{"jsonData": '+data+'}]', '*');
			break;
		case 'activateHome':
			$("#content header").addClass("active-home");
			$("#content header").removeClass("active-sync");
			break;
		case 'activateSync':
			$("#content header").addClass("active-sync");
			$("#content header").removeClass("active-home");
			break;
		case 'disableSync':
			$("#content header").removeClass("active-sync");
			break;
		case 'returnHome':
			returnHome();
			break;
		case 'getLatLong': // location
			nativeInterface.getLatLong();
			// getLatLongCallback("14.5901216,121.0604752");
			break;
		case 'goTo':
			nativeInterface.socialMediaLink(message[0].apiMediaLink.toString(),message[0].mediaLink.toString());
			break;
		case 'sync':
			var fileToSync = message[0].fileName;
			nativeInterface.sync(fileToSync);
			break;
		case 'error':
			nativeInterface.msgBox(message[0].errorMsg, "Error");
			break;
		default:
			break;
	}
}

function navigate(e){
	if (e.target != e.currentTarget && (e.target.id+".html") != $iframe[0].src) {
		if (e.target.id != "qrcode" && e.target.id != "points") {
			$iframe[0].src = e.target.id+".html";
			setFrameHeader(e.target.id)
			$body.addClass('show-iframe');
		}else{
			if (nativeInterface.loggedIn() == "true") {
				$iframe[0].src = e.target.id+".html";
				setFrameHeader(e.target.id)
				$body.addClass('show-iframe');
			}else{
				$body.addClass('show-login');
			}
		}
	}
}

function setFrameHeader(page){
	// OBJECT: KEY is page argument, VALUE is header title
	var titles = {"about": "About", "exclusives": "Exclusives", "location": "Locations", "product": "Products", "points":"Points", "qrcode": "My Code"}
	$("#content header > .title").html(titles[page]);
}

function backs(e){
	if ($body.hasClass('show-reg')) {
		$("input").val('').blur();
		$("select option[value='']").attr('selected', true);
		$body.removeClass('show-reg');
	}else if ($body.hasClass('show-login')){
		$("input").val('').blur();
		$body.removeClass('show-login');
	}else{
		$iframe[0].contentWindow.postMessage('[{"keyEvent": "return"}]', '*')
	}
}

function returnHome(e){
	$body.removeClass();
	$("input").val('').blur();
	setTimeout(function(){
		$iframe[0].src = "";
		$("#content header").removeClass().addClass("active-sync");
	},500);
}

function sync(e){
	var fileToSync = $(e.target)[0].getAttribute("data-sync");
	nativeInterface.sync(fileToSync.toString());
}

// Login/Registration functions

function initializeDOB(){
	for (i = new Date().getFullYear(); i > 1939; i--){
		var currentYear = new Date();
	    $('#years').append($('<option />').val(i).html(i));   
	}
	    
	for (i = 1; i < 13; i++){
		var mon = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sept ","Oct","Nov","Dec"];
	    $('#months').append($('<option />').val(i).html(mon[i-1]));
	}

	updateNumberOfDays(); 
	    
    $('#years, #months').change(function(){
        updateNumberOfDays(); 
    });
}


function updateNumberOfDays(){
	$('#days').html('<option value="">Day</option>');
	month=$('#months').val();
	year=$('#years').val();
	days=daysInMonth(month, year);

    for(i=1; i < days+1 ; i++){
        $('#days').append($('<option />').val(i).html(i));
    }

}

function daysInMonth(month, year) {
    return new Date(year, month, 0).getDate();
}

function register(e){
	var email = $("#regEmail").val(); 
	var pass = $("#regPass").val();
	var cpass = $("#cpass").val();

	var fname = $("#fname").val();
	var mname = $("#mname").val();
	var lname = $("#lname").val();

	var address = $("#address").val();
	var street = $("#street").val();
	var city = $("#city").val();
	var postal = $("#postal").val();

	var number = $("#mobileNum").val();
	
	var year = $("#years option:selected").val()
	var month = $("#months option:selected").val()
	var day = $("#days option:selected").val()
	var dob = year +"-"+ month +"-"+ day;
	var gender = $("#gender option:selected").val();
	
	var specialExp = /((^[0-9]+)|(^[a-z]+))+[0-9a-z]+$/i;
	var digitExp = /[0-9]/;
	var lwrCaseExp = /[a-z]/;
	var uprCaseExp = /[A-Z]/;


	if(email == ""){
		$("#email").focus();
		nativeInterface.msgBox('Please enter your email','Error');
	}else if(!validateEmail(email)){
		$("#email").focus();
		nativeInterface.msgBox('Please enter a valid email','Error');
	}else if(pass == ""){
		$("#password").focus();
		nativeInterface.msgBox('Please enter your password','Error');
	}else if(pass.length < 4){
		$("#password").focus();
		nativeInterface.msgBox('Password must be at least 4 characters long and must contain at least 1 number, at least 1 uppercase letter, and at least 1 lowercase letter. Special characters are not allowed.','Error');
	}else if(pass != cpass){
		$("#password").focus();
		nativeInterface.msgBox('Passwords do not match','Error');
	}else if(!pass.match(specialExp)){
		$("#password").focus();
		nativeInterface.msgBox('Password must be at least 4 characters long and must contain at least 1 number, at least 1 uppercase letter, and at least 1 lowercase letter. Special characters are not allowed.','Error');
	}else if(!digitExp.test(pass)){
		$("#password").focus();
		nativeInterface.msgBox('Password must be at least 4 characters long and must contain at least 1 number, at least 1 uppercase letter, and at least 1 lowercase letter. Special characters are not allowed.','Error');
	}else if(!lwrCaseExp.test(pass)){
		$("#password").focus();
		nativeInterface.msgBox('Password must be at least 4 characters long and must contain at least 1 number, at least 1 uppercase letter, and at least 1 lowercase letter. Special characters are not allowed.','Error');
	}else if(!uprCaseExp.test(pass)){
		$("#password").focus();
		nativeInterface.msgBox('Password must be at least 4 characters long and must contain at least 1 number, at least 1 uppercase letter, and at least 1 lowercase letter. Special characters are not allowed.','Error');
	}else if (fname == "") {
		$("#fname").focus();
		nativeInterface.msgBox('Please enter your first name!','Error');
	}else if(lname == ""){
		$("#lname").focus();
		nativeInterface.msgBox('Please enter your last name','Error');
	}else if(address == ""){
		$("#address").focus();
		nativeInterface.msgBox('Please enter your address','Error');
	}else if(number == ""){
		$("#number").focus();
		nativeInterface.msgBox('Please enter your number','Error');
	}else if(!validateNumber(number)){
		$("#number").focus();
		nativeInterface.msgBox('Please enter a valid number','Error');
	}else if(year == ""){
		nativeInterface.msgBox('Please select your year of birth','Error');
	}else if(month == ""){
		nativeInterface.msgBox('Please select your month of birth','Error');
	}else if(day == ""){
		nativeInterface.msgBox('Please select your day of birth','Error');
	}else if(gender == ""){
		nativeInterface.msgBox('Please select your gender','Error');
	}else{
		$("input").blur();
		nativeInterface.registration('[{"email":"'+email+'", "fname":"'+fname+'", "mname":"'+mname+'", "lname":"'+lname+'", "address1":"'+address+'", "dateOfBirth":"'+dob+'", "password":"'+pass+'", "mobileNum":"'+number+'", "gender": "'+gender+'", "address2": "'+street+'", "city": "'+city+'", "zip": "'+postal+'" }]');
	}
}

function login(e){
	var email = $("#logEmail").val();
	var password = $("#logPass").val();

	if (email == '') {
		$("#logEmail").focus();
		nativeInterface.msgBox('Please enter your email','Error');
	}else if(!validateEmail(email)){
		$("#logEmail").focus();
		nativeInterface.msgBox('Please enter a valid email','Error');
	}else if(password == ''){
		$("#logPass").focus();
		nativeInterface.msgBox('Please enter your password','Error');
	}else{
		$("input:focus").blur();
		nativeInterface.loginUserWithPW(email, password);
	}
}

// form validation
function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function validateNumber(number){
	number = number.split(" ").join("");
	number = number.split(".").join("");
	number = number.split("-").join("");
	number = (number.charAt(0) == "+") ? number.slice(1,number.length) : number;
	var length = (number.substr(0,2) == "63") ? 12 : 11;

	if (!isNaN(number)) {
		if (number.length == length) {
			if (length == 12) {
				number = number.replace("63","0");
				return true;
			}else{
				if (number.substr(0,2) == "09") {
					return true;
				}
			}
		}
	}

	return false;	
}

// callback
function getLatLongCallback(position){ // for location
	if (position != "0.0,0.0") {
		$iframe[0].contentWindow.postMessage('[{"keyEvent": "getDirections", "latLong": "'+position+'"}]', '*');
	}else{
		nativeInterface.msgBox('Can\'t get your current location! ','Error');
	}
}

function registrationCallback(){
	$("input").val('').blur();
	$body.removeClass('show-reg');
}

function loginCallback(){
	$("input").val('').blur();
	$body.removeClass('show-login');
}

function syncCallback(){
	$iframe[0].contentWindow.postMessage('[{"keyEvent": "refresh"}]', '*');
}


// native back flag
function shouldExit(){
	if ($body.hasClass('show-iframe') || $body.hasClass('show-login') || $body.hasClass('show-reg')) {
		return "false";
	}else{
		return "true";
	}
}




