
// SELECTORS
var $locations;
var $locList;
var $directionPanel;

// MAPS
var clongitude;
var clatitude;
var map;
var marker;
var locPosition;
var currentPosition;
var directionsService;
var directionsDisplay;
var mapInitTimer;
var directionsTimer;

// FLAGS
var isInitialized = false;
var isDirectionMode = false;
var validCoordinates = false;
var mapStatus = false;
var suppressedDir = false;
$(function(){
    $locations = $(".loc-list");
    $locList = $(".loc-list .list-wrapper");
    $address = $("#add");
    $locName = $("#loc");
    $directionPanel = $("#directions-panel");
    addEventListeners();
    init(LOCATION);

    // var data = '[{"locID": "LOCASD1ASD2aAkhn","address": "Bldg. G San Miguel by the Bay, SM Mall of Asia Seaside Blvd,  SM Mall Of Asia, Pasay City","latitude": "14.536390","longitude": "120.979257","locName": "Mall of Asia","phone": "804-0130","email": "bsimoa@seafood-island.com","status": "active","locFlag": "active"},{"locID": "LOCASD1ASD2aAkhn","address": "Fiesta Market, Market! Market! Bonifacio Global City, Taguig City","latitude": "14.551314","longitude": "121.055975","locName": "Market!Market!","phone": "886-7692","email": "bsimarketmarket@seafood-island.com","status": "active","locFlag": "active"}]';

    // parseData(data);
});

// CORE
function addEventListeners(){
    FastClick.attach( document.body ); // removes 300ms delay

    $locList.on('click', chooseLocation);
    $("#dir").on('click', getPosition);
    $("#call").on('click', call);
    $("#mail").on('click', mail);
    $("#empty div").on('click', function(e){ reInit(); });
    window.addEventListener("message", messageHandler, false);
}

function messageHandler(e){
    var message = JSON.parse(e.data);
    var keyEvent = message[0].keyEvent;
    switch(keyEvent){
        case 'jsonData':
            var data = message[1].jsonData;
            if (data.length > 0 && typeof data != "undefined") {
                $("#listPage").addClass("active-page");
                parseData(data);
            }else{
                $("#empty").addClass("active-page");
            }
            break;
        case 'return':
            if ($(".active-page")[0].id == "mapPage" ) {
                revertMapDetails();
                $(".active-page").removeClass("active-page");
                $("#listPage").addClass("active-page");
                activateSync();
            }else{
                parent.postMessage('[{"keyEvent": "returnHome"}]', '*')
            }
            break;
        case 'getDirections':
            getDirections((message[0].latLong).toString());
            break;
        case 'refresh':
            window.location.reload();
        default:
          break;
    }
}

function parseData(data){
    $locList.empty();
    var jsonData = data;
    var fragment = document.createDocumentFragment();
    for (var i = 0; i < jsonData.length; i++) {
        var item = document.createElement("li");
        item.id = "item_"+i;
        item.setAttribute("data-location",JSON.stringify(jsonData[i]));
        item.innerHTML = jsonData[i].locName;
        fragment.appendChild(item);
        item = null;
    }
    $locList.append(fragment);
    fragment = null;
}

function revertMapDetails(){
    $("#dir").removeClass('active-dir').html("Directions");
    $directionPanel.empty().hide();
    initializeMap();
    isDirectionMode = false;
}

// HANDLER
function chooseLocation(e){
    if (e.target.tagName == "LI" && e.target.id.split("_")[0] == "item") {
        showMap(e.target.getAttribute("data-location"));
    }
}

function getPosition(e){
    parent.postMessage('[{"keyEvent": "getLatLong"}]', '*');
}

function getDirections(position){
    if (isInitialized) {
        var lat = position.split(",")[0];
        var lon = position.split(",")[1];
        
        clearTimeout(directionsTimer);
        if (!isDirectionMode) {
            directionsTimer = setTimeout(function(){
                if (mapStatus != google.maps.DirectionsStatus.OK) {
                    parent.postMessage('[{"keyEvent": "error", "errorMsg": "Failed to get directions!"}]', '*');
                    suppressedDir = true;
                }
            },20000);

            directionsService = new google.maps.DirectionsService();
            directionsDisplay = new google.maps.DirectionsRenderer();
            
            currentPosition = new google.maps.LatLng(lat, lon);
            directionsDisplay.setMap(map);
            var request = {
                origin: currentPosition,
                destination: locPosition,
                travelMode: google.maps.DirectionsTravelMode["DRIVING"]
            };

            directionsService.route(request, function(response, status){
                mapStatus = status;
                if (status == google.maps.DirectionsStatus.OK) {
                    if (!suppressedDir) {
                        clearTimeout(directionsTimer);
                        directionsDisplay.setDirections(response);
                        directionsDisplay.setPanel($directionPanel[0]);
                        isDirectionMode = true;
                        $directionPanel.show();
                        $("#dir").addClass('active-dir').html("Branch");
                    }else{
                        suppressedDir = false;
                    }
                }else{
                    if (!suppressedDir) {
                        clearTimeout(directionsTimer);
                        parent.postMessage('[{"keyEvent": "error", "errorMsg": "Unable to Calculate Route to destination."}]', '*');
                    }else{
                        suppressedDir = false;
                    }
                }
            });
           
        }else{
            revertMapDetails();
        }
    }else{
        parent.postMessage('[{"keyEvent": "error", "errorMsg": "Can\'t connect to maps! Please check your internet connection"}]', '*');
    }
}

function call(e){ 
    if($(e.target).data("phone") == ""){
        parent.postMessage('[{"keyEvent": "error", "errorMsg": "Can\'t call this branch!"}]', '*');
    }else{
        window.location.href = "tel://"+$(e.target).data("phone");
    } 
}
function mail(e){ 
    if($(e.target).data("email") == ""){
        parent.postMessage('[{"keyEvent": "error", "errorMsg": "Can\'t email this branch!"}]', '*');     
    }else{
        window.location.href = "mailto://"+$(e.target).data("email");
    } 
}

// MAPS
function loadMapScript() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'http://maps.google.com/maps/api/js?sensor=true&callback=initializeMap';
    $("#map-canvas").find('.failed-loading').remove();
    document.body.appendChild(script);
    mapInitTimer = setTimeout(
        function(){
            if (!isInitialized) { 
                $("#map-canvas").html('<div class="failed-loading">click TO RETRY</div>');
                $(".failed-loading").on('click', loadMapScript);
                $("body script").remove();
                parent.postMessage('[{"keyEvent": "error", "errorMsg": "Can\'t connect to google maps! Please check your internet connection"}]', '*'); 
            }

        }, 10000);
}

function initializeMap() {
    if (window.google === undefined) {
        isInitialized = false;
    }else{
        clearTimeout(mapInitTimer);
        locPosition = new google.maps.LatLng(clatitude, clongitude);
        mapOptions = {
            zoom: 14,
            center: locPosition
        };
        map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
        marker = new google.maps.Marker({
            position: locPosition,
            map: map,
            title: 'Store Location'
        });
        
        isInitialized = true;
    }
}


function showMap(locData){
    var jsonData = JSON.parse(locData);
    clatitude = jsonData.latitude;
    clongitude = jsonData.longitude;
    
    if(!isInitialized){
        loadMapScript();
    }else{
        validCoordinates = ((clatitude == "0.0" || clatitude == 0.0) && (clongitude == "0.0" || clongitude == 0.0)) ? false : true;
        locPosition = new google.maps.LatLng(clatitude, clongitude);
        map.setCenter(locPosition)
        marker.setPosition(locPosition);
    }

    document.querySelector("#mail").setAttribute("data-email",jsonData.email);
    document.querySelector("#call").setAttribute("data-phone",jsonData.phone);
    
    activateHome();
    $(".active-page").removeClass("active-page");
    $("#mapPage").addClass("active-page");

    $address.text(jsonData.address);
    $locName.text(jsonData.locName);
}

