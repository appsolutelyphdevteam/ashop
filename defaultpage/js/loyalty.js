
$(function(){
	init(LOYALTY);
	addEventListeners();
});

function addEventListeners(){
	FastClick.attach( document.body ); // removes 300ms delay

	// $(".list-wrapper").on('click', showCampaign);
	$("#emptyState").on('click', function(e){ reInit() });
	window.addEventListener("message", messageHandler, false);
}

function messageHandler(e){
	var message = JSON.parse(e.data);
	var keyEvent = message[0].keyEvent;
	switch(keyEvent){
		case 'jsonData':
            var data = message[1].jsonData;
			if (data.length > 0 && typeof data != "undefined") {
				$(".page:first-child").addClass("active-page");
				parseData(message[1].jsonData);
			}else{
				$(".page:last-child").addClass("active-page");
			}
			break;
		case 'return':
			parent.postMessage('[{"keyEvent": "returnHome"}]', '*');
			break;
		case 'refresh':
			window.location.reload();
			break;
		default:
			break;
	}
}

// function showCampaign(e){
// 	if (e.target.tagName == "LI") {
// 		activateBack();
// 		disableSync();
// 		$(".active-content").removeClass("active-content");
// 		$(".loyalty-details").addClass("active-content");

// 		var userPoints = $(".points").data("points");
// 		var campData = $(e.target).data("campaign");
// 		var status = ( userPoints >= campData.points) ? "YOU CAN NOW REDEEM" : "YOU NEED "+(campData.points-userPoints)+" STAMP(S)";
// 		var fragment = document.createDocumentFragment();
// 		$(".stamp-wrapper").empty();
// 		for (var i = 0; i < campData.points; i++) {
// 			var img = new Image();
// 			var random = Math.floor((Math.random() * 6));
// 			img.src = srcArr[random];
// 			img.className = (userPoints < i+1) ? "faded" : "";
// 			fragment.appendChild(img);
// 			img = null;
// 			random = null;
// 		};
// 		$(".stamp-wrapper").append(fragment);
// 		$("#campStatus").html(status);
// 		$("#campName").html(campData.name);
// 		$("#campDesc").html(campData.description);

// 		userPoints = null;
// 		campData = null;
// 		status = null;
// 		fragment = null;

// 	}
// }

function parseData(data){
	var jsonData = data;
	var profileData = data[1].profile;
	var campaignData = data[0].campaign;

	// SET PROFILE
	if(profileData.totalPoints > 1) {$(".points div")[0].setAttribute("data-label", "POINTS")} else {$(".points div")[0].setAttribute("data-label", "POINT")}
	$(".points div").text(profileData.totalPoints);
	document.querySelector(".points").setAttribute("data-points", profileData.totalPoints);
	
	// SET CAMPAIGN 
	$(".list-wrapper").empty();
	if (campaignData.length > 0) {
		var fragment = document.createDocumentFragment();
		campaignData.sort(function(a,b){ return a.points - b.points; });
		for (var i = 0; i < campaignData.length; i++) {
			var li = document.createElement("li"),
				points = document.createElement("div"),
				name = document.createElement("div");
				li.id = "item_"+i;
				// li.setAttribute("data-campaign", JSON.stringify(campaignData[i]));
				points.innerHTML = campaignData[i].points;
				points.setAttribute("data-label", (parseInt(campaignData[i].points) > 1) ? "POINTS" : "POINT" );
				name.innerHTML = campaignData[i].name;
				li.appendChild(points);
				li.appendChild(name);
				fragment.appendChild(li);
				li = null;
				points = null;
				name = null;
		}
		$(".list-wrapper").append(fragment);
		fragment = null;
	}else{
		$(".list-wrapper").html('<div class="no-campaign">No Campaigns Available!</div>')
	}

	profileData = null;
	campaignData = null;
}