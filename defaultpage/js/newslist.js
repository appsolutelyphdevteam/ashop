$(function(){
    // var data = [{"id": "2","image": "css/images/news-1.jpg","startDate": "07\/07\/1990","time1": "1:30","switch1": "1","endDate": "07\/08\/1990","time2": "2:00","switch2": "0","title": "Jomac Boodle","status": "1","description": "","dateAdded": "Jul-11-2015","status": "active"}];
    
    
    addEventListeners();
    init(NEWS);

});

function addEventListeners(){
    FastClick.attach( document.body ); // removes 300ms delay

    $(".list-wrapper").on('click',showDetails);
    $("#empty div").on('click', function(e){ reInit(); });
    window.addEventListener("message", messageHandler, false);
}

function messageHandler(e){
    var message = JSON.parse(e.data);
    var keyEvent = message[0].keyEvent;
    switch(keyEvent){
        case 'jsonData':
            var data = message[1].jsonData;
            if (data.length > 0 && typeof data != "undefined") {
                $("#listPage").addClass("active-page");
                parseData(message[1].jsonData);
            }else{
                $("#empty").addClass("active-page");
            }
            break;
        case 'return':
            if ($(".active-page")[0].id == "detailPage" ) {
                $(".active-page").removeClass("active-page");
                $("#listPage").addClass("active-page");
                activateSync();
            }else{
                parent.postMessage('[{"keyEvent": "returnHome"}]', '*');
            }
            break;
        case 'refresh':
            window.location.reload();
            break;
        default:
            break;
    }

}

function showDetails(e){
    if (e.target.tagName == "LI") {
        var data = $(e.target).data("news");
        activateHome();
        var img = new Image();
        img.src = data.image;
        img.addEventListener('load', function(){
            document.querySelector("#newsImg").src = $(this)[0].src;
            $(this)[0] = null;
        });
        img.addEventListener('error', function(){ $(this)[0] = null; });
        $(".active-page").removeClass("active-page");
        $("#detailPage").addClass("active-page");
        $("#newsTitle").html(data.title);
        $("#newsDetails").html(data.description);
        data = null;
    }
}

function parseData(data){
    $(".list-wrapper").empty();
    var jsonData = data;
    var fragment = document.createDocumentFragment();
    for (var i = 0; i < jsonData.length; i++) {
        var li = document.createElement("li"),
            name = document.createElement("div");

            name.innerHTML = jsonData[i].title;
            li.setAttribute("data-news", JSON.stringify(jsonData[i]));
            li.appendChild(name);
            fragment.appendChild(li);
            
            name = null;
            li = null;
    }
    $(".list-wrapper").append(fragment);
    fragment = null;
    jsonData = null;
}