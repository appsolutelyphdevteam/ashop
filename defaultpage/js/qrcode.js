$(function(){
    init(LOYALTY);

    window.addEventListener('message', messageHandler, false);
});

function messageHandler(e){
	var message = JSON.parse(e.data);
	var keyEvent = message[0].keyEvent;
	switch(keyEvent){
		case 'jsonData':
			var data = message[1].jsonData;
            if (data.length > 0 && typeof data != "undefined") {
                $(".page:first-child").addClass("active-page");
               	data = data[1].profile;
				var qr = new QRCode(document.getElementById("qr"), {
			        width: 200,
			        height: 200
			    });
			    qr.makeCode(data.qrApp);
			    $("#name").html(data.fname+" "+data.mname+" "+data.lname);
			    $("#email").html(data.email);
            }else{
                $(".page:last-child").addClass("active-page");
            }
			break;
		case 'return':
			parent.postMessage('[{"keyEvent": "returnHome"}]', '*');
			break;
		case 'refresh':
			window.location.reload();
			break;
		default:
			break;
	}
}