// var mainCatergory = {0: "BOODLE FEAST", 1: "FRESH CATCH", 2: "ALA CARTE", 3: "DRINKS & DESSERTS", 4: "CRABS & SHELLFISH", 5: "FEATURED PRODUCTS"};
var completeData;
var arrangement = []
// SELECTORS
var $mainMenu;
var $subMenu;
var $subList;
var $item;
var $detail;

$(function(){
	$mainMenu = $("#main");
	$subMenu = $("#sub");
	$subList = $(".list-wrapper"); 
	$item = $("#item");
	$detail = $(".detail");
	addEventListeners();
	init(PRODUCT);

	// parseData(data);
	// completeData = data;
});

function addEventListeners(){
	FastClick.attach( document.body ); // removes 300ms delay

	$("#main ul").on('click', selectCategory);
	$(".dlist-wrapper").on('click', showDetails);
	$("#empty div").on('click', function(e){ reInit(); });

	window.addEventListener("message", messageHander, false);
}

function messageHander(e){
	var message = JSON.parse(e.data);
	var keyEvent = message[0].keyEvent;
	switch(keyEvent){
		case 'jsonData':
			var data = message[1].jsonData;
			if (data.length > 0 && typeof data != "undefined") {
				$("#main").addClass('active-page');
				generateGrid(data);
			}else{
				$("#empty").addClass('active-page');
			}
			break;
		case 'return':
			if($(".active-page")[0].id == "sub" ){
				$(".active-page").removeClass('active-page');
				activateSync();
				$("#main").addClass('active-page');
			}else{
				parent.postMessage('[{"keyEvent": "returnHome"}]', '*');
			}
			break;
		case 'refresh':
			window.location.reload();
			break;
		default:
			break;
	}
}

function generateGrid(data){
	// var arr = [ 4, 2, 0, 1, 5, 3];
	var arr = {"Mac": 0, "iPhone": 1, "iPad": 2, "iPod": 3, "Apple Tv": 4, "Accessories": 5};
	completeData = _.sortBy(data, function(o){ return arr[o.name] } );
	var imgArr = [];
	var fragment = document.createDocumentFragment();
	for (var i = 0; i < completeData.length; i++) {
		var li = document.createElement('li');
		var div = document.createElement('div');
		div.id = "cat_"+i;
		imgArr[i] = new Image();
		imgArr[i].id = "img_"+i;
		imgArr[i].src = completeData[i].image;
		imgArr[i].addEventListener('load',function(){ $("#cat_"+$(this)[0].id.split('_')[1]).css('background-image', 'url('+$(this)[0].src+')') });
		imgArr[i].addEventListener('error',function(){ $("#cat_"+$(this)[0].id.split('_')[1])[0].setAttribute('data-label',$("#cat_"+$(this)[0].id.split('_')[1])[0].getAttribute('data-onerr') ) });
		li.setAttribute('data-key', completeData[i].name);
		div.setAttribute('data-onerr', completeData[i].name);
		li.appendChild(div);
		fragment.appendChild(li);
	}
	$(".grid-wrapper").append(fragment);
	fragment = null;
}


function selectCategory(e){
	if (e.target != e.currentTarget) {
		var index = _.findIndex(completeData,{ name: $(e.target).data("key") });
		var data = completeData[index];
		if (index != -1 ) {
			$(".active-page").removeClass('active-page');
			$("#sub").addClass('active-page');
			$(".sub-header").html($(e.target).find('div').data("label"))
			activateHome();
			generateDlist(data);
		}else{
			parent.postMessage('[{"keyEvent": "error", "errorMsg": "There are no items in this category yet!"}]', '*');
			// alert("WALANG LAMAN!")
		}
	}
}

function generateDlist(data){
	var imgArr = [];
	var fragment = document.createDocumentFragment();
	$(".dlist-wrapper").empty();
	for (var i = 0; i < data.record.length; i++) {
		var record = data.record[i];
		var li = document.createElement("li");
		var name = document.createElement("div");
		var desc = document.createElement("div");
		var descTarget = document.createElement("div");
		imgArr[i] = new Image();
		$(imgArr[i]).hide();
		imgArr[i].className = 'item-image';
		imgArr[i].src = record.image;
		imgArr[i].addEventListener('load',function(){ $(this).show(); });
		imgArr[i].addEventListener('error',function(){ $(this).hide(); });
		name.className = 'item-name';
		name.innerHTML = record.name;
		desc.className = 'item-detail';
		desc.innerHTML = record.description;
		descTarget.className = 'item-toggle';
		li.appendChild(imgArr[i]);
		li.appendChild(name);
		li.appendChild(desc);
		li.appendChild(descTarget);
		fragment.appendChild(li);
	}
	$(".dlist-wrapper").append(fragment);
	fragment = null;
}

function viewDetails(e){
	if (e.target != e.currentTarget) {
		$(".image-container").empty();
		var categoryData = $subList.data("category");
		var categoryIndex = categoryData.cIndex;
		var index = $(e.target).index();
		var img = new Image();
		$(".active-content").removeClass("active-content");
		$(".details").addClass("active-content");
		img.src = completeData[categoryIndex].items[index].image;
		img.addEventListener('error', function(e){
			$(".image-container").empty();
		}, false);
		$(".image-container").append(img);
		$item.text(completeData[categoryIndex].items[index].name);
		$detail.html(completeData[categoryIndex].items[index].description);
	}
}

function showDetails(e){
	if (e.target != e.currentTarget && e.target.className == "item-toggle") {

		if(!$(e.target).parent().hasClass('active')){
			$(".active").removeClass('active');
			$(e.target).parent().addClass('active');
		}else{
			$(".active").removeClass('active');
		}
		
	}
}

